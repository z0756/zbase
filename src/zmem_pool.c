/*
* Smart IP Camera
* Copyright (c) BZ <bzchen@allwinnertech.com>
*
* This file is part of SIPC.
*/
#include <zmem_pool.h>
#include <zlog.h>
#include <zlist.h>
#include <zatomic.h>

#include <pthread.h>
#include <stdlib.h>
#include <errno.h>

#define POOL_BLOCK_SIZE 8192
#define POOL_ALIGNMENT 8
#define POOL_LARGE_SIZE 4095

#define ZAlign(d, a) (((d) + (a - 1)) & ~(a - 1)) /*���϶���*/

struct PoolDataS
{
    char *last;
    char *end;
    ZListNodeT node;
    ZListT pmList;
    z_atomic_t ref;
    int failed;
};

struct MemPoolS
{
    struct PoolDataS *current;
    ZListT largeList; /* large PoolMemoryS list */
    ZListT pdList; /* PoolDataS list */
    ZListT childList; /* child pool list */
    ZListNodeT node; /* in father's child list */
    pthread_mutex_t mutex;
    char *file;
    int line;
};

struct PoolMemoryS
{
    struct PoolDataS *owner;
    char *file;
    int line;
    ZListNodeT node;
    int size;
};

static MemPoolT *gGolbalPool = NULL;

static inline struct PoolDataS *PoolDataIncRef(struct PoolDataS *poolData)
{
    ZAtomicInc(&poolData->ref);
    return poolData;
}

static inline void PoolDataDecRef(struct PoolDataS *poolData)
{
    if (ZAtomicDec(&poolData->ref) == 0)
    {
        ZListDel(&poolData->node);
        free(poolData);
    }
}

static void *PallocBlock(MemPoolT *pool, int size, char *file, int line)
{
    struct PoolDataS *pd = NULL, *newPd = NULL, *currentPd = NULL;
    ZListNodeT *pbNode = NULL, *nPbNode = NULL;   
    struct PoolMemoryS *pm = NULL;

    newPd = malloc(POOL_BLOCK_SIZE);
//    newPd = memalign(POOL_ALIGNMENT, POOL_BLOCK_SIZE);
    if (newPd == NULL)
    {
        ZLogE("memalign alloc %d failure errno(%d)", POOL_BLOCK_SIZE, errno);
        return NULL;
    }

    newPd->end = ((char *)newPd) + POOL_BLOCK_SIZE;
    newPd->last = ((char *)newPd) + ZAlign(sizeof(struct PoolDataS), POOL_ALIGNMENT);
    newPd->failed = 0;
    ZListInit(&newPd->pmList);
    ZAtomicSet(&newPd->ref, 1);

    for (pbNode = &pool->current->node, nPbNode = pbNode->next;
         pbNode != (ZListNodeT *)&pool->pdList; 
         pbNode = nPbNode, nPbNode = pbNode->next)
    {
        pd = ZListEntry(pbNode, struct PoolDataS, node);
        if (pd->failed++ > 4)
        {
            if (pd->node.next != (ZListNodeT *)&pool->pdList)
            {
                currentPd = ZListEntry(pd->node.next, struct PoolDataS, node);
            }
            else
            {
                currentPd = newPd;
            }
            PoolDataDecRef(pd);
        }
    }

    ZListAddTail(&newPd->node, &pool->pdList);
    pool->current = currentPd ? currentPd : pool->current;

    pm = (struct PoolMemoryS *)newPd->last;
    pm->owner = newPd;
    pm->file = file;
    pm->line = line;
    pm->size = size;
    
    newPd->last += ZAlign(sizeof(*pm) + size, POOL_ALIGNMENT);
    ZListAddTail(&pm->node, &newPd->pmList);
    PoolDataIncRef(newPd);
    
    return pm + 1;
}

static void *PallocLarge(MemPoolT *pool, int size, char *file, int line)
{
    struct PoolMemoryS *pm;

    pm = malloc(size + sizeof(*pm));
    if (pm == NULL) 
    {
        ZLogE("malloc size(%d) failure, errno(%d)", size, errno);
        return NULL;
    }

    pm->owner = NULL;
    pm->file = file;
    pm->line = line;
    pm->size = size;
    ZListAdd(&pm->node, &pool->largeList);
    return pm + 1;
}

static MemPoolT *PoolNodeCreate(char *file, int line)
{
    MemPoolT *pool; 
    struct PoolDataS *poolData;

    pool = malloc(1024);
    poolData = malloc(POOL_BLOCK_SIZE);

//    pool = memalign(POOL_ALIGNMENT, 1024);
//    poolData = memalign(POOL_ALIGNMENT, POOL_BLOCK_SIZE);
    if ((!pool) || (!poolData)) 
    {
        ZLogE("memalign alloc %d failure errno(%d)", POOL_BLOCK_SIZE, errno);
		if(pool)
			free(pool);
		if(poolData)
			free(poolData);
        return NULL;
    }

    poolData->last = (char *) poolData + sizeof(*poolData);
    poolData->end = (char *) poolData + POOL_BLOCK_SIZE;
    poolData->failed = 0;
    ZListInit(&poolData->pmList);
    
    ZListInit(&pool->largeList);
    ZListInit(&pool->pdList);
    pool->current = poolData;
    ZListInit(&pool->childList);

    pthread_mutexattr_t attr;
    if (pthread_mutexattr_init(&attr) != 0)
    {
        ZLogE("init thread mutex attr failure...");
        return NULL;
    }
    if (pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE) != 0)
    {
        ZLogE("pthread_mutexattr_settype failure...");
        return NULL;
    }
    pthread_mutex_init(&pool->mutex, &attr);

    ZListAddTail(&poolData->node, &pool->pdList);
    ZAtomicSet(&poolData->ref, 1); /*pool.data*/

    pool->file = file;
    pool->line = line;

    return pool;
}

MemPoolT *__MemPoolCreate(MemPoolT *father, char *file, int line)
{
    MemPoolT  *pool;
    if (!father)
    {
        if (!gGolbalPool)
        {
            gGolbalPool = PoolNodeCreate(__FILE__, __LINE__);
        }
        father = gGolbalPool;
    }

    pool = PoolNodeCreate(file, line);
    
    ZListAdd(&pool->node, &father->childList);
    
    return pool;
}

void MemPoolDestroy(MemPoolT *pool)
{
    MemPoolT *p, *nP;
    struct PoolMemoryS *pm, *nPm;
    struct PoolDataS *pd, *nPd;

    /*destroy child  pool*/
    ZListForEachEntrySafe(p, nP, &pool->childList, node)
    {
        ZListDel(&p->node);
        MemPoolDestroy(p);
    }

    if (pool != gGolbalPool)
    {
        ZListDel(&pool->node); /* cut from father's list */
    }
    
    ZListForEachEntrySafe(pm, nPm, &pool->largeList, node)
    {
        ZLogE("memory leak @<%s:%d>", strrchr(pm->file, '/') + 1, pm->line);        
        ZListDel(&pm->node);
        free(pm);
    }

    ZListForEachEntrySafe(pd, nPd, &pool->pdList, node)
    {
        if (pd->failed > 5)
        {
            PoolDataIncRef(pd);
        }
        
        if (ZAtomicRead(&pd->ref) != 1)
        {
            struct PoolMemoryS *pm;
            ZListForEachEntry(pm, &pd->pmList, node)
            {
                ZLogE("memory leak @<%s:%d>", strrchr(pm->file, '/') + 1, pm->line);        
                PoolDataDecRef(pd);
            }
        }
        
        Z_LOG_ASSERT(ZAtomicRead(&pd->ref) == 1, "ref(%d), failed(%d)", 
                    ZAtomicRead(&pd->ref), pd->failed);
        PoolDataDecRef(pd);
    }

    pthread_mutex_destroy(&pool->mutex);

    free(pool);
    return ;
}

void *MemPalloc(MemPoolT *pool, int size, char *file, int line)
{
    struct PoolDataS *pd;
    int pmSize;
    void *ret;
        
    if (!pool)
    {
        if (!gGolbalPool)
        {
            gGolbalPool = PoolNodeCreate(file, line);
        }
        pool = gGolbalPool;
    }
    pthread_mutex_lock(&pool->mutex);
    
    pmSize = ZAlign(sizeof(struct PoolMemoryS) + size, POOL_ALIGNMENT);
    
    if (pmSize <= POOL_LARGE_SIZE) 
    {
        ZListNodeT *pbNode = NULL;
        struct PoolMemoryS *pm = NULL;
    	for (pbNode = &pool->current->node;
             pbNode != (ZListNodeT *)&pool->pdList; 
    	     pbNode = pbNode->next)
        {
            pd = ZListEntry(pbNode, struct PoolDataS, node);
            if ((int)(pd->end - pd->last) >= pmSize)
            {
                pm = (struct PoolMemoryS *)pd->last; 
                pm->owner = pd;
                pm->file = file;
                pm->line = line;
                pm->size = size;
                PoolDataIncRef(pm->owner);
                ZListAddTail(&pm->node, &pd->pmList);
                pd->last += pmSize;
                ret = pm + 1;
                goto out;
            }
        }

        ret = PallocBlock(pool, size, file, line);
        goto out;
    }

    ret = PallocLarge(pool, size, file, line);

out:
    pthread_mutex_unlock(&pool->mutex);
    return ret;
}

void *MemRealloc(MemPoolT *pool, void *p, int size, char *file, int line)
{
    struct PoolMemoryS *pm;
    int freeSize = 0;
    void *newP;
    
    if (!pool)
    {
        pool = gGolbalPool;
    }
    
    pthread_mutex_lock(&pool->mutex);

    pm = ((struct PoolMemoryS *)p) - 1;

    Z_LOG_ASSERT(size > pm->size, "invalid size, (%d, %d)", size, pm->size);
    
    if (pm->size > POOL_LARGE_SIZE) /*in large memory*/
    {
        newP = PallocLarge(pool, size, file, line);
        if (!newP)
        {
            ZLogE("realloc failure...");
            goto out;
        }
        memcpy(newP, p, pm->size);
        ZListDel(&pm->node);
        free(pm);
        goto out;
    }

    if (size > POOL_LARGE_SIZE)
    {
        newP = PallocLarge(pool, size, file, line);
        if (!newP)
        {
            ZLogE("realloc failure...");
            goto out;
        }
        memcpy(newP, p, pm->size);
        MemPfree(pool, p);
        goto out;
    }
    
    if (pm->node.next == (ZListNodeT *)&pm->owner->pmList)
    {
        freeSize = ((char *)(pm->owner)) + POOL_BLOCK_SIZE - ((char *)p);
    }
    else
    {
        struct PoolMemoryS *nextPm;
        nextPm = ZListEntry(pm->node.next, struct PoolMemoryS, node);
        freeSize = ((char *)nextPm) - ((char *)p);
    }
    
    if (freeSize >= size)
    {
        pm->size = size;
        pm->file = file;
        pm->line = line;
        newP = p;
        if (pm->node.next == (ZListNodeT *)&pm->owner->pmList)
        {
            pm->owner->last = ((char *)pm) + ZAlign(sizeof(struct PoolMemoryS) + size, POOL_ALIGNMENT);
        }
        goto out;
    }

    newP = MemPalloc(pool, size, file, line);
    memcpy(newP, p, pm->size);
    MemPfree(pool, p);

out:
    pthread_mutex_unlock(&pool->mutex);    
    return newP;
}

void MemPfree(MemPoolT *pool, void *p)
{
    struct PoolMemoryS *pm;
    
    if (!pool)
    {
        pool = gGolbalPool;
    }

    pthread_mutex_lock(&pool->mutex);

    pm = ((struct PoolMemoryS *)p) - 1;
    if (pm->size > POOL_LARGE_SIZE)
    {
        ZListDel(&pm->node);
        free(pm);
    }
    else
    {
        ZListDel(&pm->node);
        PoolDataDecRef(pm->owner);
    }

    pthread_mutex_unlock(&pool->mutex);    
    return ;
}

void MemPoolReset(void)
{
    if (gGolbalPool)
    {
        MemPoolDestroy(gGolbalPool);
        gGolbalPool = NULL;
    }
    else
    {
        ZLogW("global pool not initinal...");
    }
    return ;
}

