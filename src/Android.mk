LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
    zlog.c     \
    zmem_pool.c     \
    zqueue.c       \
    zbuffer.c       \
    zbit_reader.c       \
    zutils.c

LOCAL_C_INCLUDES  := \
        $(LOCAL_PATH)/include/


LOCAL_MODULE_TAGS := optional

LOCAL_MODULE:= libzbase

LOCAL_SHARED_LIBRARIES +=   \
        libutils            \
        libcutils

include $(BUILD_SHARED_LIBRARY)

