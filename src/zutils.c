/*
* Smart IP Camera
* Copyright (c) BZ <bzchen@allwinnertech.com>
*
* This file is part of SIPC.
*/

#include <stdlib.h>
#include <sys/time.h>
//#include <execinfo.h>
#include <ztypes.h>
#include <zlog.h>

int64_t ZGetNowUs(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);

    return (int64_t)tv.tv_usec + tv.tv_sec * 1000000ll;
}

void ZBTDump(void)
{
    /*
    int ret, i;
    void *buffer[128] = {NULL};
    char **bt;
    
    ret = backtrace(buffer, 128);
    bt = backtrace_symbols(buffer, ret);
    CDX_LOGW("-----------------BT START. (%d) (%p)---------------", ret, bt);
    for (i = 0; i < ret; i++)
    {
        CDX_LOGW("<DEBUG>: %s", bt[i]);
    }
    
    CDX_LOGW("-----------------BT END.---------------");
*/
    return ;
}

