/*
* Smart IP Camera
* Copyright (c) BZ <bzchen@allwinnertech.com>
*
* This file is part of SIPC.
*/
#include <stdio.h>
#include <stdarg.h>

#include <zlog.h>

#include <syslog.h>
#include <assert.h>

void ZLog(int prio, const char *tag, const char *fmt, ...)
{
	static char *log_prio_map[5] =
		{
			"D",
			"I",
			"W",
			"E",
			"F"
		};
	char logbuf[1024] = {0};
	
    va_list args;

    va_start(args, fmt);
    vsnprintf(logbuf, 1024, fmt, args);
    va_end(args);

/* tina */
#ifdef UBUNTU_LINUX	
	printf("%s|%s\n", log_prio_map[prio], logbuf);   
#else
    syslog(LOG_ERR, "%s|%s\n", log_prio_map[prio], logbuf);   
#endif
    return ;
}

void ZAssertFail(const char *cond)
{

	ZLogE("assert '%s' fail.", cond);
	assert(0);
	
	return;
}

