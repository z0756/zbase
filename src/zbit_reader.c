#define ZLOG_TAG "ZBitReader"

#include <zlog.h>
#include <stdlib.h>
#include <zbit_reader.h>

struct ZBitReaderS
{
    const uint8_t *mData;
    uint32_t mSize;
    uint32_t mReservoir;  // left-aligned bits
    uint32_t mNumBitsLeft;
};

/* Big endian */
static void onFillReservoir(ZBitReaderT *br)
{
    uint32_t i;

    Z_ASSERT(br->mSize > 0u);

    br->mReservoir = 0;
    for (i = 0; br->mSize > 0 && i < 4; ++i) 
	{
        br->mReservoir = (br->mReservoir << 8) | *(br->mData);

        ++(br->mData);
        --(br->mSize);
    }

    br->mNumBitsLeft = 8 * i;
    br->mReservoir <<= 32 - br->mNumBitsLeft;
    return ;
}

uint32_t ZBR_GetBits(ZBitReaderT *br, uint32_t n)
{
    Z_ASSERT(br);

    uint32_t result = 0;
    Z_ASSERT(n <= 32u);

    while (n > 0)
	{
        uint32_t m;
        if (br->mNumBitsLeft == 0) 
		{
            onFillReservoir(br);
        }

        m = n;
        if (m > br->mNumBitsLeft) 
        {
            m = br->mNumBitsLeft;
        }

        result = (result << m) | (br->mReservoir >> (32 - m));
        br->mReservoir <<= m;
        br->mNumBitsLeft -= m;

        n -= m;
    }

    return result;
}

void ZBR_SkipBytes(ZBitReaderT *br, uint32_t n)
{
    Z_ASSERT(br->mNumBitsLeft%8 == 0);
    uint32_t skip = n;
    
    if (br->mNumBitsLeft)
    {
        if (skip > (br->mNumBitsLeft/8))
        {
            ZBR_GetBits(br, br->mNumBitsLeft);
            skip -= (br->mNumBitsLeft/8);
        }
        else
        {
            ZBR_GetBits(br, skip * 8);
            goto out;
        }
    }

    br->mData += skip;
    br->mSize -= skip;
    
out:
    return ;
}

void ZBR_SkipBits(ZBitReaderT *br, uint32_t n)
{
    Z_ASSERT(br);
	
    while (n > 32)
    {
        ZBR_GetBits(br, 32);
        n -= 32;
    }

    if (n > 0)
    {
        ZBR_GetBits(br, n);
    }
    return ;
}

uint32_t ZBR_NumBitsLeft(ZBitReaderT *br)
{

    return br->mSize * 8 + br->mNumBitsLeft;
}

const uint8_t *ZBR_Data(ZBitReaderT *br)
{
    return br->mData - (br->mNumBitsLeft + 7) / 8;
}

ZBitReaderT *ZBR_Create(const uint8_t *data, uint32_t size)
{
    struct ZBitReaderS *br;
    br = malloc(sizeof(*br));
    Z_ASSERT(br);
	
    br->mData = data;
    br->mSize = size;
    br->mReservoir = 0;
    br->mNumBitsLeft = 0;

    return br;
}

void ZBR_Destroy(ZBitReaderT *br)
{
    Z_ASSERT(br);
    free(br);
    return ;
}

