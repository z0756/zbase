/*
* Smart IP Camera
* Copyright (c) BZ <bzchen@allwinnertech.com>
*
* This file is part of SIPC.
*/
#include <ztypes.h>
#include <zatomic.h>

#include <zlog.h>
#include <zqueue.h>

#define ZAtomicBoolCAS(ptr, oldVal, newVal) \
            __sync_bool_compare_and_swap(ptr, oldVal, newVal)

struct ZQueueImplS
{
    struct ZQueueS base;
    struct QueueNodeEntityS *front;
    struct QueueNodeEntityS *rear;
    int32_t enablePop;
    int32_t enablePush;
    MemPoolT *pool;
};

struct QueueNodeEntityS
{
    struct QueueNodeEntityS *next;
    z_atomic_t ref;
    ZQueueDataT data;
};

static inline struct QueueNodeEntityS *
        QueueNodeEntityIncRef(struct QueueNodeEntityS *entity)
{
    ZAtomicInc(&entity->ref);
    return entity;
}

static inline void QueueNodeEntityDecRef(MemPoolT *pool, struct QueueNodeEntityS *entity)
{
    if (ZAtomicDec(&entity->ref) == 0)
    {
        Pfree(pool, entity);
    }
}


static ZQueueDataT __ZQueuePop(ZQueueT *queue)
{
    struct ZQueueImplS *impl;
    struct QueueNodeEntityS *entity = NULL;
    ZQueueDataT data;
    Z_ASSERT(queue);
    impl = ZContainerOf(queue, struct ZQueueImplS, base);

    if (!impl->enablePop)
    {
        return NULL;
    }
    
    do
    {
        if (entity)
        {
            QueueNodeEntityDecRef(impl->pool, entity);
        }
        entity = QueueNodeEntityIncRef(impl->front);
        if (entity->next == NULL)
        {
            QueueNodeEntityDecRef(impl->pool, entity);
            return NULL;
        }
        data = entity->next->data; 
        /*
              *先把数据保存下来，
              *避免取到entity之后，它的next被释放了
              */
    }
    while (!ZAtomicBoolCAS(&impl->front, entity, entity->next));
    QueueNodeEntityDecRef(impl->pool, entity); /*对应上面取entity的时候+1*/
    QueueNodeEntityDecRef(impl->pool, entity); /*再-1 才能释放内存*/
    
    return data;
}

static int32_t __ZQueuePush(ZQueueT *queue, ZQueueDataT data)
{
    struct ZQueueImplS *impl;
    struct QueueNodeEntityS *entity, *tmpEntity;
    int32_t ret;
    
    Z_ASSERT(queue);
    impl = ZContainerOf(queue, struct ZQueueImplS, base);
    
    if (!impl->enablePush)
    {
        return -1;
    }
    entity = Palloc(impl->pool, sizeof(*entity));
    Z_ASSERT(entity);
    Z_ASSERT(data);/*不希望有为0的data*/
    entity->data = data;
    entity->next = NULL;
    ZAtomicSet(&entity->ref, 1);

    do
    {
        tmpEntity = impl->rear;
    }
    while (!ZAtomicBoolCAS(&tmpEntity->next, NULL, entity));

    ret = ZAtomicBoolCAS(&impl->rear, tmpEntity, entity);
    Z_ASSERT(1 == ret);

    return 0;
}

static int32_t __ZQueueEmpty(ZQueueT *queue)
{
    struct ZQueueImplS *impl;
        
    Z_ASSERT(queue);
    impl = ZContainerOf(queue, struct ZQueueImplS, base);
    
    return impl->front == impl->rear;
}

static struct ZQueueOpsS gQueueOps =
{
    .pop = __ZQueuePop,
    .push = __ZQueuePush,
    .empty = __ZQueueEmpty
};

ZQueueT *ZQueueCreate(MemPoolT *pool)
{
    struct ZQueueImplS *impl;
    struct QueueNodeEntityS *dummy;
    impl = Palloc(pool, sizeof(struct ZQueueImplS));
    Z_ASSERT(impl);
    memset(impl, 0x00, sizeof(struct ZQueueImplS));

    impl->pool = pool;
    dummy = Palloc(impl->pool, sizeof(struct QueueNodeEntityS));
    Z_ASSERT(dummy);
    dummy->next = NULL;
    dummy->data = NULL;
    ZAtomicSet(&dummy->ref, 1);
    
    impl->front = dummy;
    impl->rear = dummy;
    impl->base.ops = &gQueueOps;
    impl->enablePop = 1;
    impl->enablePush = 1;
    return &impl->base;
}

void ZQueueDestroy(ZQueueT *queue)
{
    struct ZQueueImplS *impl;
//    struct ZQueueNodeEntityS *dummy;
    
    Z_ASSERT(queue);
    impl = ZContainerOf(queue, struct ZQueueImplS, base);

    impl->enablePush = 0;
    
    impl->enablePop = 1;
    Z_LOG_ASSERT(impl->front == impl->rear, "queue not empty");

    QueueNodeEntityDecRef(impl->pool, impl->front);
    Pfree(impl->pool, impl);
	impl = NULL;

    return ;
}

