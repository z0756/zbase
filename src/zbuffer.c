
#include <stdlib.h>

#define ZLOG_TAG "ZBuffer"
#include <zlog.h>
#include <zatomic.h>
#include <zbuffer.h>

struct ZBufferS
{
    z_atomic_t ref;
    uint8_t *ptr;
    int size;
    struct ZBufferMemInfS *memops;
    MemPoolT *pool;
};

static void onBufferDestroy(ZBufferT * buf)
{
    Z_ASSERT(buf);
    Z_ASSERT(buf->ptr);
    Z_ASSERT(buf->memops);

    buf->memops->memfree(buf->ptr);
    Pfree(buf->pool, buf);
    
    return;
}

ZBufferT *ZBufferIncref(ZBufferT * buf)
{
    ZAtomicInc(&buf->ref);
    return buf;
}

int ZBufferDecref(ZBufferT * buf)
{
    if (ZAtomicDec(&buf->ref) == 0)
    {
        onBufferDestroy(buf);
    }
    return 0;
}

int ZBufferDestroy(ZBufferT * buf)
{
    Z_ASSERT(ZAtomicRead(&buf->ref) == 1);
    ZBufferDecref(buf);
    return 0;
}

uint8_t *ZBufferGetbufptr(ZBufferT *buf)
{
    return buf->ptr;
}

int ZBufferGetbufsize(ZBufferT *buf)
{
    return buf->size;
}

ZBufferT *ZBufferCreate(MemPoolT *pool, struct ZBufferMemInfS *meminf, size_t size)
{
    struct ZBufferS *buf = NULL;

    Z_ASSERT(meminf);
    buf = Palloc(pool, sizeof(*buf));
    if (!buf)
    {
        ZSimpleErrMark();
        return NULL;
    }
    
    buf->pool = pool;
    buf->memops = meminf;
    buf->size = size;
    buf->ptr = buf->memops->memalloc(size);
    if (!buf->ptr)
    {
        ZLogE("alloc mem failure, size '%u')", size);
        goto err_out;
    }

    ZAtomicSet(&buf->ref, 1);

    return buf;

err_out:
    if (buf)
    {
        Pfree(pool, buf);
    }
    return NULL;
}

ZBufferT *STDBufferCreate(size_t size)
{
    static struct ZBufferMemInfS std_meminf =
    {
        .memalloc = malloc,
        .memfree = free
    };
    static MemPoolT *pool = NULL;

    /* Even though will create duplicate when mutil call first, But it's OK, so no need to lock it */
    if (!pool) 
    {
        pool = MemPoolCreate(NULL);
        if (!pool)
        {
            ZSimpleErrMark();
            return NULL;
        }
    }
    
    return ZBufferCreate(pool, &std_meminf, size);
}


