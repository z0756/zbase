#ifndef ZLOG_H
#define ZLOG_H

#ifndef ZLOG_TAG
#define ZLOG_TAG "UNDEFINE"
#endif

#ifndef LOG_LEVEL
#define LOG_LEVEL Z_LOG_DEBUG
#endif

enum ZLogLevelE
{
    Z_LOG_DEBUG = 0,
    Z_LOG_INFO,
    Z_LOG_WARNING,
    Z_LOG_ERROR,
    Z_LOG_FAULT,
};

#ifdef __cplusplus
extern "C" 
{
#endif

void ZLog(int prio, const char *tag, const char *fmt, ...);

void ZAssertFail(const char *cond);

#ifdef __cplusplus
}
#endif

#define IF_LOG(prio, fmt, arg...)                                       \
    do {                                                                \
        if (prio >= LOG_LEVEL)                                          \
        {                                                               \
			ZLog(prio, ZLOG_TAG, "<%s:%d>:"fmt, __FUNCTION__, __LINE__, ##arg);           \
        }                                                               \
    } while (0)

#define ZLogD(fmt, arg...) \
			IF_LOG(Z_LOG_DEBUG, fmt, ##arg)

#define ZLogI(fmt, arg...) \
			IF_LOG(Z_LOG_INFO, fmt, ##arg)

#define ZLogW(fmt, arg...) \
			IF_LOG(Z_LOG_WARNING, fmt, ##arg)

#define ZLogE(fmt, arg...) \
			IF_LOG(Z_LOG_ERROR, "\033[40;31m"fmt"\033[0m", ##arg)

#define ZLogF(fmt, arg...) \
			IF_LOG(Z_LOG_FAULT, "\033[40;31m"fmt"\033[0m", ##arg)

#define ZSimpleErrMark() ZLogE("something error here...")

#define Z_LOG_ASSERT(e, fmt, arg...)                            \
    do {                                                        \
        if (!(e))                                               \
        {                                                       \
			ZLogF("assert(%s) fail,"fmt, #e, ##arg);				\
            ZAssertFail(#e);   									\
        }                                                       \
    } while (0)

#define Z_ASSERT(e) Z_LOG_ASSERT(e, "@ '%s:%d'", __FILE__, __LINE__)

#define Z_UNUSE(x) (void)x

#endif

