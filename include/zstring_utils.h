/*
* Copyright (c) BZ <bzchen@allwinnertech.com>
*
*/

#ifndef Z_STRING_UTILS_H
#define Z_STRING_UTILS_H

#ifdef __cplusplus
extern "C"
{
#endif
#include <ztypes.h>

/* key=value */
static int STR_GetAttrByKey(char *content, char *delimiter, char *key, char *value)
{
	char *pos = content;
	int key_len = strlen(key);
	int content_len = strlen(content);
	int ret = 0;
	char *pos_delimiter = NULL;
	int delimiter_len = strlen(delimiter);
	
	while (pos - content < content_len)
	{
		char *equal_sign = strchr(pos, '=');
		if (!equal_sign)
		{
			ret = -1;
			ZLogE("invalid data: '%s'", content);
			break;
		}

		char *pos_delimiter = strstr(pos, delimiter);
		if (!pos_delimiter)
		{
			ret = -1;
			ZLogE("invalid data: '%s'", content);
			break;
		}

		if ((equal_sign-pos == key_len) && strncmp(key, pos, key_len) == 0)
		{
			int value_len = pos_delimiter - equal_sign - 1;
			memcpy(value, equal_sign+1, value_len);
			value[value_len] = 0;
			ret = 0;
			break;
		}

		pos = pos_delimiter + delimiter_len;
	}

	return ret;
}


#ifdef __cplusplus
}
#endif

#endif
