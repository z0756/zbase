/*
* Smart IP Camera
* Copyright (c) BZ <bzchen@allwinnertech.com>
*
* This file is part of SIPC.
*/
#ifndef Z_MEM_POOL_H
#define Z_MEM_POOL_H

#include <ztypes.h>
#include <string.h>

typedef struct MemPoolS MemPoolT;

#define Palloc(pool, size) \
    MemPalloc(pool, size, __FILE__, __LINE__)

#define Pfree(pool, p) \
    MemPfree(pool, p)

#define Pstrdup(pool, str)   \
    MemStrDup(pool, str, __FILE__, __LINE__)

#define Prealloc(pool, p, size) \
    MemRealloc(pool, p, size, __FILE__, __LINE__)
    
#ifdef __cplusplus
extern "C"
{
#endif

MemPoolT *__MemPoolCreate(MemPoolT *father, char *file, int line);

#define MemPoolCreate(father) __MemPoolCreate(father, __FILE__, __LINE__)

void MemPoolDestroy(MemPoolT *pool);

void *MemPalloc(MemPoolT *pool, int size, char *file, int line);

void MemPfree(MemPoolT *pool, void *p);

void MemPoolReset(void);

static inline char *MemStrDup(MemPoolT *pool, const char *str, char *file, int line)
{
    char *ret = NULL;
    int strLen = 0;
//    CDX_ASSERT(str);

    strLen = strlen(str);
    
    ret = (char *)MemPalloc(pool, strLen + 1, file, line);
    strcpy(ret, str);
    ret[strLen] = 0;
    return ret;
}

void *MemRealloc(MemPoolT *pool, void *p, int size, char *file, int line);

#ifdef __cplusplus
}
#endif

#endif /* Z_MEM_POOL_H */

