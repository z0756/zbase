/*
* Copyright (c) BZ <bzchen@allwinnertech.com>
*
*/

#ifndef Z_NET_UTILS_H
#define Z_NET_UTILS_H

#ifdef __cplusplus
extern "C"
{
#endif
#include <ztypes.h>
#include <fcntl.h>
#include <netinet/tcp.h>

static int AsyncSK_Create(int domain)
{
    int sockfd;
    int ret;

    sockfd = socket(domain, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        ZLogE("errno(%d)", errno);
        return -1;
    }

    // Make socket non-blocking.
    int flags = fcntl(sockfd, F_GETFL, 0);

    if (flags == -1)
    {
        return -1;
    }


	flags |= O_NONBLOCK;
    flags = fcntl(sockfd, F_SETFL, flags);

	if (flags == -1)
	{
		return -1;
	}

    return sockfd;
}

static int AsyncSK_KeepAlive(int sk)
{
	int keepalive = 1;							// 开启keepalive属性
	int keepidle = 10;			// 如该连接在60秒内没有任何数据往来,则进行探测
	int keepinterval = 5;	// 探测时发包的时间间隔为5 秒
	// 探测尝试的次数.如果第1次探测包就收到响应了,则后2次的不再发.
	int keepcount = 3;
	
    setsockopt(sk, SOL_SOCKET, SO_KEEPALIVE, (void *)&keepalive , sizeof(keepalive));
    setsockopt(sk, SOL_TCP, TCP_KEEPIDLE, (void*)&keepidle , sizeof(keepidle));
    setsockopt(sk, SOL_TCP, TCP_KEEPINTVL, (void *)&keepinterval , sizeof(keepinterval));
    setsockopt(sk, SOL_TCP, TCP_KEEPCNT, (void *)&keepcount , sizeof(keepcount));
	return 0;
}

static int AsyncSock_Recv(int sk, char *buf, int capability)
{
    int ret = 0, len = 0;
    
    while (1)
    {
        ret = recv(sk, buf + len, capability - len, MSG_DONTWAIT);
        if (ret < 0)
        {
            if (EAGAIN == errno || EWOULDBLOCK == errno)
            {
                /* noblocking socket */
                break;
            }
            else
            {
                len = -1;
                ZLogE("recv failure, errno(%d) ", errno);
                break;
            }
        }
        else if (ret == 0) /* close by peer...?? */
        {
            ZLogE("recv error, errno(%d)", errno);
            break;
        }
        else /* ret > 0 */
        {
            len += ret;
        }

        if (len == capability)
        {
            /* buf full */
            break;
        }
    }

    return len;
}

static int AsyncSock_Send(int sk, const char *buf, int len)
{
    int ret = 0, done = 0;
    while (1)
    {
        ret = send(sk, buf + done, len - done, MSG_DONTWAIT);
        if (ret < 0)
        {
            if (EAGAIN == errno || EWOULDBLOCK == errno)
            {
                //buffer not ready
                break;
            }
            else
            {
                ZLogE("send failure, errno(%d)", errno);
                return -1;
            }
        }
        else if (ret == 0)
        {
            //buffer is full?
            break;
        }
        else // ret > 0
        {
            done += ret;
        }
        
        if (done == len)
        {
            /* finish... */
            break;
        }
    }
    
    return done;
}

#ifdef __cplusplus
}
#endif

#endif
