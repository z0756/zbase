#ifndef Z_BUFFER_H
#define Z_BUFFER_H

#include <stdint.h>
#include <zmem_pool.h>

typedef struct ZBufferS ZBufferT;

struct ZBufferMemInfS
{
    void *(*memalloc)(size_t /*size*/);
    void (*memfree)(void * /*ptr*/);
};

#ifdef __cplusplus
extern "C" {
#endif

ZBufferT *ZBufferIncref(ZBufferT * buf);

int ZBufferDecref(ZBufferT * buf);

int ZBufferDestroy(ZBufferT * buf);

uint8_t *ZBufferGetbufptr(ZBufferT *buf);

int ZBufferGetbufsize(ZBufferT *buf);

ZBufferT *ZBufferCreate(MemPoolT *pool, struct ZBufferMemInfS *meminf, size_t size);

ZBufferT *STDBufferCreate(size_t size);

#ifdef __cplusplus
}
#endif

#endif
