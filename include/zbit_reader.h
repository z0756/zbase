
#ifndef ZBIT_READER_H
#define ZBIT_READER_H
#include <stdint.h>

typedef struct ZBitReaderS ZBitReaderT;


#ifdef __cplusplus
extern "C"
{
#endif

ZBitReaderT *ZBR_Create(const uint8_t *data, uint32_t size);

void ZBR_Destroy(ZBitReaderT *br);

uint32_t ZBR_GetBits(ZBitReaderT *br, uint32_t n);

void ZBR_SkipBits(ZBitReaderT *br, uint32_t n);

void ZBR_SkipBytes(ZBitReaderT *br, uint32_t n);

void ZBR_PutBits(ZBitReaderT *br, uint32_t x, uint32_t n);

uint32_t ZBR_NumBitsLeft(ZBitReaderT *br);

const uint8_t *ZBR_Data(ZBitReaderT *br);


#ifdef __cplusplus
}
#endif

#endif
