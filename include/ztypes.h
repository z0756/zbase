/*
* Smart IP Camera
* Copyright (c) BZ <bzchen@allwinnertech.com>
*
* This file is part of SIPC.
*/

#ifndef Z_TYPES_H
#define Z_TYPES_H

#include <stdint.h>

#define ZOffsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER) 

#define ZContainerOf(ptr, type, member) ({ \
    const typeof(((type *)0)->member) *__mptr = (ptr); \
    (type *)((char *)__mptr - ZOffsetof(type,member) ); })

#define ZINTERFACE static inline

#endif
