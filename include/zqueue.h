/*
* Smart IP Camera
* Copyright (c) BZ <bzchen@allwinnertech.com>
*
* This file is part of SIPC.
*/

#ifndef Z_QUEUE_H
#define Z_QUEUE_H
#include <ztypes.h>
#include <zlog.h>
#include <zmem_pool.h>

typedef struct ZQueueS ZQueueT;
typedef void * ZQueueDataT;

struct ZQueueOpsS
{
    ZQueueDataT (*pop)(ZQueueT *);
    int32_t (*push)(ZQueueT *, ZQueueDataT);
    int32_t (*empty)(ZQueueT *);  /* 0 : empty, 1 : not empty */
    
};

struct ZQueueS
{
    struct ZQueueOpsS *ops;
};

static inline ZQueueDataT ZQueuePop(ZQueueT *queue)
{
    Z_ASSERT(queue);
    Z_ASSERT(queue->ops);
    Z_ASSERT(queue->ops->pop);
    return queue->ops->pop(queue);
}

static inline int32_t ZQueuePush(ZQueueT *queue, ZQueueDataT data)
{
    Z_ASSERT(queue);
    Z_ASSERT(queue->ops);
    Z_ASSERT(queue->ops->push);
    return queue->ops->push(queue, data);
}


static inline int32_t ZQueueEmpty(ZQueueT *queue)
{
    Z_ASSERT(queue);
    Z_ASSERT(queue->ops);
    Z_ASSERT(queue->ops->empty);
    return queue->ops->empty(queue);
}

#ifdef __cplusplus
extern "C"
{
#endif
/*this is a look free queue*/
ZQueueT *ZQueueCreate(MemPoolT *pool);

void ZQueueDestroy(ZQueueT *queue);

#ifdef __cplusplus
}
#endif

#endif
