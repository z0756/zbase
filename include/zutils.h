/*
* Smart IP Camera
* Copyright (c) BZ <bzchen@allwinnertech.com>
*
* This file is part of SIPC.
*/

#ifndef Z_UTILS_H
#define Z_UTILS_H

#ifdef __cplusplus
extern "C"
{
#endif
#include <ztypes.h>

int64_t ZGetNowUs();

void ZBTDump(void);

static inline uint32_t GetU8(const uint8_t *data)
{
    return (uint32_t)(*data);
}

static inline uint32_t GetLE16(const uint8_t *data)
{
    return GetU8(data) | (GetU8(data + 1) << 8);
}

static inline uint32_t GetLE32(const uint8_t *data)
{
    return GetLE16(data) | (GetLE16(data + 2) << 16);
}

static inline uint64_t GetLE64(const uint8_t *data)
{
    return ((uint64_t)GetLE32(data)) | (((uint64_t)GetLE32(data + 4)) << 32);
}

static inline uint32_t GetBE16(const uint8_t *data)
{
    return (GetU8(data) << 8) | GetU8(data + 1);
}

static inline uint32_t GetBE32(const uint8_t *data)
{
    return (GetBE16(data) << 16) | GetBE16(data + 2);
}

static inline uint64_t GetBE64(const uint8_t *data)
{
    return (((uint64_t)GetBE32(data)) << 32) | ((uint64_t)GetBE32(data + 4));
}

#ifdef __cplusplus
}
#endif

#endif
