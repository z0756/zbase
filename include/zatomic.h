/*
* Smart IP Camera
* Copyright (c) BZ <bzchen@allwinnertech.com>
*
* This file is part of SIPC.
*/
#ifndef Z_ATOMIC_H
#define Z_ATOMIC_H

#include <ztypes.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct 
{
    volatile long counter;
} z_atomic_t;

/*引用计数+ 1 ，返回结果的数值*/
static inline int32_t ZAtomicInc(z_atomic_t *ref) 
{
    return __sync_add_and_fetch(&ref->counter, 1L);
}

static inline int32_t ZAtomicAdd(z_atomic_t *ref, long val)
{
    return __sync_add_and_fetch(&ref->counter, val);
}

/*引用计数- 1 ，返回结果的数值*/
static inline int32_t ZAtomicDec(z_atomic_t *ref)
{
    return __sync_sub_and_fetch(&ref->counter, 1L);
}

static inline int32_t ZAtomicSub(z_atomic_t *ref, long val)
{
    return __sync_sub_and_fetch(&ref->counter, val);
}

/*设置引用计数为val，返回设置前数值*/
static inline int32_t ZAtomicSet(z_atomic_t *ref, long val)
{
    return __sync_lock_test_and_set(&ref->counter, val);
}

/*读取引用计数数值*/
static inline int32_t ZAtomicRead(z_atomic_t *ref)
{
    return __sync_or_and_fetch(&ref->counter, 0L);
}

static inline int32_t ZAtomicCAS(z_atomic_t *ref, long oldVal, long newVal)
{
    return __sync_bool_compare_and_swap(&ref->counter, oldVal, newVal);
}



#ifdef __cplusplus
}
#endif

#endif
